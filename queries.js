const Pool = require("pg").Pool;
const pool = new Pool({
  user: "fadilah",
  host: "localhost",
  database: "nyc",
  password: "password",
  port: 5432,
});

const getMaps = (request, response) => {
  pool.query("SELECT * FROM file_storage ORDER BY id ASC", (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};

const uploadFile = (request, response) => {
  console.log("requesttt body", request.body);
  const { file, name } = request.body;
  pool.query(
    `insert into file_storage (name, file) values ($1, $2) RETURNING *`,
    [name, file],
    (error, results) => {
      if (error) {
        throw error;
      }
      response.status(201).send(`File added: ${results.rows[0].id}`);
    }
  );
};

module.exports = {
  getMaps,
  uploadFile,
};
