const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const db = require("./queries");

const app = express();
const port = 3000;

app.use(cors());

app.use(bodyParser.json({ limit: "50mb" }));
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "50mb",
  })
);

app.get("/", (request, response) => {
  response.json({ info: "Node.js, Express, and Postgres API" });
});

app.get("/maps", db.getMaps);
app.post("/upload", db.uploadFile);

app.listen(port, () => {
  console.log(`App running on port ${port}.`);
});
